#!/bin/bash
echo on    

echo ''###########################################################''
echo '#Script para clonar os repositorios da PortoCred.         #'
echo '#Falta terminar                                           #'
echo ''###########################################################''
echo '#Autor: Yan Wesley                                         #'
echo ''###########################################################''
cd ..
echo '###########################################################'
echo Clonando API Callback.
git clone git@bitbucket.org:portocred-financeira/api-callback.git
cd ./api-callback
git checkout develop
cd ..
echo '###########################################################'
echo Clonando api-efetivacao-automatica
git clone git@bitbucket.org:portocred-financeira/api-efetivacao-automatica.git
cd ./api-efetivacao-automatica
git checkout develop
cd ..
echo '###########################################################'
echo Clonando SiteCP
git clone git@bitbucket.org:portocred-financeira/sitecp.git
cd ./sitecp
git checkout develop
cd ..
echo '###########################################################'
echo Clonando api-calculadora
git clone git@bitbucket.org:portocred-financeira/api-calculadora.git
cd ./api-calculadora
git checkout develop
cd ..
echo '###########################################################'
echo Clonando API Pagamentos
git clone git@bitbucket.org:portocred-financeira/api-pagamentos.git
cd ./api-pagamentos
git checkout develop
cd ..
echo '###########################################################'
echo Clonando API Inadimplencia
git clone git@bitbucket.org:portocred-financeira/api-inadimplencia.git
cd ./api-inadimplencia
git checkout develop
cd ..
echo '###########################################################'
echo Clonando WcfPropostas
git clone git@bitbucket.org:portocred-financeira/wcfpropostas.git
cd ./wcfpropostas
git checkout develop
cd ..
echo '###########################################################'
echo Clonando gerenciador-propostas-painel
git clone git@bitbucket.org:portocred-financeira/gerenciador-propostas-painel.git
cd ./gerenciador-propostas-painel
git checkout develop
cd ..
echo '###########################################################'
echo Clonando api-pre-propostas-varejo
git clone git@bitbucket.org:portocred-financeira/api-pre-propostas-varejo.git
cd ./api-pre-propostas-varejo
git checkout develop
cd ..
echo '###########################################################'
echo Clonando consultar-contrato-refin
git clone git@bitbucket.org:portocred-financeira/consultar-contrato-refin.git
cd ./consultar-contrato-refin
git checkout develop
cd ..
echo '###########################################################'
echo Clonando motor-ofertas-core
git clone git@bitbucket.org:portocred-financeira/motor-ofertas-core.git
cd ./motor-ofertas-core
git checkout develop
cd ..
echo '###########################################################'
echo Clonando portocred-api-simulacao
git clone git@bitbucket.org:portocred-financeira/portocred-api-simulacao.git
cd ./portocred-api-simulacao
git checkout develop
cd ..
echo '###########################################################'
echo Clonando gerenciador-propostas
git clone git@bitbucket.org:portocred-financeira/gerenciador-propostas.git
cd ./gerenciador-propostas
git checkout develop
cd ..
echo '###########################################################'
echo Clonando API-Garantia-CDC
git clone git@bitbucket.org:portocred-financeira/api-garantia-cdc.git
cd ./api-garantia-cdc
git checkout develop
cd ..
echo '###########################################################'
echo Clonando WCFImpressaoDocumentos
git clone git@bitbucket.org:portocred-financeira/wcfimpressaodocumentos.git
cd ./wcfimpressaodocumentos
git checkout develop
cd ..
echo '###########################################################'
echo Clonando API-Propostas
git clone git@bitbucket.org:portocred-financeira/api-propostas.git
cd ./api-propostas
git checkout develop
cd ..
echo '###########################################################'
echo Clonando Portocred.Sicred
git clone git@bitbucket.org:portocred-financeira/portocred.sicred.git
cd ./portocred.sicred
git checkout develop
cd ..
echo '###########################################################'
echo '#Repositorios clonado com sucesso!                        #'
echo '###########################################################'
sleep 10s
