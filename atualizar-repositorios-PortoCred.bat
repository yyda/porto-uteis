@echo on    
@echo '###########################################################'
@echo '#Script para atualizar os repositorios do projeto da rumo.#'
@echo '###########################################################'
@echo '#Autor: Yan Wesley                                        #'
@echo '###########################################################'
@echo '###########################################################'
@echo '###########################################################'
cd ..
@echo Atualizando API Callback
cd ./api-callback
git pull
cd ..
@echo '###########################################################'
@echo Atualizando api-efetivacao-automatica
cd ./api-efetivacao-automatica
git pull
cd ..
@echo '###########################################################'
@echo Atualizando SiteCP
cd ./sitecp
git pull
cd ..
@echo '###########################################################'
@echo Atualizando api-calculadora
cd ./api-calculadora
git pull
cd ..
@echo '###########################################################'
@echo Atualizando API Pagamentos
cd ./api-pagamentos
git pull
cd ..
@echo '###########################################################'
@echo Atualizando API Inadimplencia
cd ./api-inadimplencia
git pull
cd ..
@echo '###########################################################'
@echo Atualizando WcfPropostas
cd ./wcfpropostas
git pull
cd ..
@echo '###########################################################'
@echo Atualizando gerenciador-propostas-painel
cd ./gerenciador-propostas-painel
git pull
cd ..
@echo '###########################################################'
@echo Atualizando api-pre-propostas-varejo
cd ./api-pre-propostas-varejo
git pull
cd ..
@echo '###########################################################'
@echo Atualizando consultar-contrato-refin
cd ./consultar-contrato-refin
git pull
cd ..
@echo '###########################################################'
@echo Atualizando motor-ofertas-core
cd ./motor-ofertas-core
git pull
cd ..
@echo '###########################################################'
@echo Atualizando portocred-api-simulacao
cd ./portocred-api-simulacao
git pull
cd ..
@echo '###########################################################'
@echo Atualizando API-Garantia-CDC
cd ./api-garantia-cdc
git pull
cd ..
@echo '###########################################################'
@echo Atualizando API-Propostas
cd ./api-propostas
git pull
cd ..
@echo '###########################################################'
@echo Atualizando Portocred.Sicred
cd ./portocred.sicred
git pull
cd ..
@echo '###########################################################'
@echo '#Repositorios atualizados com sucesso!                    #'
@echo '###########################################################'
timeout 10